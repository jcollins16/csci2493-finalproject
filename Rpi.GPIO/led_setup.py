import RPi.GPIO as GPIO
import time

def led_setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(17, GPIO.OUT)  # RED
    GPIO.setup(26, GPIO.OUT)  # YELLOW
    GPIO.setup(24, GPIO.OUT)  # GREEN
    GPIO.setup(13, GPIO.OUT)  # BLUE
    GPIO.setup(4, GPIO.OUT)  # WHITE
    GPIO.setup(27, GPIO.OUT)  # RED2
    GPIO.setup(6, GPIO.OUT)  # YELLOW2
    GPIO.setup(5, GPIO.OUT)  # GREEN2
    GPIO.setup(12, GPIO.OUT)  # BLUE2

led_setup()