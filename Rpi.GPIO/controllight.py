import RPi.GPIO as GPIO
import time
from sys import argv
GPIO.setwarnings(False)

whichled=argv[1]
ledaction = argv[2]
LEDa=17
LEDb=26
LEDc=24
LEDd=13
LEDe=4
LEDf=27
LEDg=6
LEDh=5
LEDi=12
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDa, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDb, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDc, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDd, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDe, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDf, GPIO.OUT)

GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDg, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDh, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDi, GPIO.OUT)

if ledaction=="off":
    if whichled=="a":
        GPIO.output(LEDa, False)
    if whichled=="b":
        GPIO.output(LEDb, False)
    if whichled=="c":
        GPIO.output(LEDc, False)
    if whichled=="d":
        GPIO.output(LEDd, False)
    if whichled=="e":
        GPIO.output(LEDe, False)
    if whichled=="f":
        GPIO.output(LEDf, False)
    if whichled=="g":
        GPIO.output(LEDg, False)
    if whichled=="h":
        GPIO.output(LEDh, False)
    if whichled=="i":
        GPIO.output(LEDi, False)
    if whichled=="all":
        GPIO.output(LEDa, False)
        GPIO.output(LEDb, False)
        GPIO.output(LEDc, False)
        GPIO.output(LEDd, False)
        GPIO.output(LEDe, False)
        GPIO.output(LEDf, False)
        GPIO.output(LEDg, False)
        GPIO.output(LEDh, False)
        GPIO.output(LEDi, False)
if ledaction=="on":
    if whichled=="a":
        GPIO.output(LEDa, True)
    if whichled=="b":
        GPIO.output(LEDb, True)
    if whichled=="c":
        GPIO.output(LEDc, True)
    if whichled=="d":
        GPIO.output(LEDd, True)
    if whichled=="e":
        GPIO.output(LEDe, True)
    if whichled=="f":
        GPIO.output(LEDf, True)
    if whichled=="g":
        GPIO.output(LEDg, True)
    if whichled=="h":
        GPIO.output(LEDh, True)
    if whichled=="i":
        GPIO.output(LEDi, True)
    if whichled=="all":
        GPIO.output(LEDa, True)
        GPIO.output(LEDb, True)
        GPIO.output(LEDc, True)
        GPIO.output(LEDd, True)
        GPIO.output(LEDe, True)
        GPIO.output(LEDf, True)
        GPIO.output(LEDg, True)
        GPIO.output(LEDh, True)
        GPIO.output(LEDi, True)