import RPi.GPIO as GPIO
import time
import led_setup
from wave import wave
from allon import all_on
from alloff import all_off



led_setup.led_setup()

wave()

all_on()
time.sleep(1)
all_off()
time.sleep(1)
all_on()
time.sleep(1)
all_off()

wave()

#GPIO.cleanup()

