from sys import argv #import the CLI command interactions
from time import sleep #import sleep() function 
from gpiozero import LED #import the gpio zero library to handle individual leds better
import RPi.GPIO #Only using gpio.cleanup() function
from gpiozero import LEDBoard #import gpio array support
from master_lightshow import * #import ALL lightshow functions

whichshow=argv[1]

leds = LEDBoard(17,26,24,13,4,27,6,5,12) #place active GPIO pins into array from 0-8

if whichshow=="blink":
    blink(leds)

if whichshow=="wave":
    wave(leds)
    revwave(leds)

if whichshow=="crisscross":
    crisscross(leds)
    revcriss(leds)

if whichshow=="inchworm":
    inchworm(leds)
    revinch(leds)

if whichshow=="misc":
    misc(leds)
    secondmisc(leds)

if whichshow=="cascade":
    cascade(leds)
    revcascade(leds)

