import RPi.GPIO as GPIO #import origionl gpio manipulation library
import time
from sys import argv #import the CLI command interactions
GPIO.setwarnings(False) #Dont give me GPIO access errors
from time import sleep #import sleep() function 
from gpiozero import LED #import the gpio zero library to handle individual leds better
import RPi.GPIO #Only using gpio.cleanup() function
from gpiozero import LEDBoard #import gpio array support
from master_lightshow import * #import ALL lightshow functions


whichled=argv[1] #Allow user input on CLI, first position; which led?

ledaction=argv[2] #allow user input on CLI, second position; led does what?

##Define LED color in relation to GPIO BCM position##
red1=17
yel1=26
gre1=24
blu1=13
whi1=4
red2=27
yel2=6
gre2=5
blu2=12


##Initilize/setup each GPIO pin##

GPIO.setmode(GPIO.BCM)
GPIO.setup(red1, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(yel1, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(gre1, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(blu1, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(whi1, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(red2, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(yel2, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(gre2, GPIO.OUT)
GPIO.setmode(GPIO.BCM)
GPIO.setup(blu2, GPIO.OUT)

if ledaction=="off":
    if whichled=="red1":
        GPIO.output(red1, False)
    if whichled=="yel1":
        GPIO.output(yel1, False)
    if whichled=="gre1":
        GPIO.output(gre1, False)
    if whichled=="blu1":
        GPIO.output(blu1, False)
    if whichled=="whi1":
        GPIO.output(whi1, False)
    if whichled=="red2":
        GPIO.output(red2, False)
    if whichled=="yel2":
        GPIO.output(yel2, False)
    if whichled=="gre2":
        GPIO.output(gre2, False)
    if whichled=="blu2":
        GPIO.output(blu2, False)
    
    if whichled=="all":
        GPIO.output(red1, False)
        GPIO.output(yel1, False)
        GPIO.output(gre1, False)
        GPIO.output(blu1, False)
        GPIO.output(whi1, False)
        GPIO.output(red2, False)
        GPIO.output(yel2, False)
        GPIO.output(gre2, False)
        GPIO.output(blu2, False)

if ledaction=="on":
    if whichled=="red1":
        GPIO.output(red1, True)
    if whichled=="yel1":
        GPIO.output(yel1, True)
    if whichled=="gre1":
        GPIO.output(gre1, True)
    if whichled=="blu1":
        GPIO.output(blu1, True)
    if whichled=="whi1":
        GPIO.output(whi1, True)
    if whichled=="red2":
        GPIO.output(red2, True)
    if whichled=="yel2":
        GPIO.output(yel2, True)
    if whichled=="gre2":
        GPIO.output(gre2, True)
    if whichled=="blu2":
        GPIO.output(blu2, True)
    
    if whichled=="all":
        GPIO.output(red1, True)
        GPIO.output(yel1, True)
        GPIO.output(gre1, True)
        GPIO.output(blu1, True)
        GPIO.output(whi1, True)
        GPIO.output(red2, True)
        GPIO.output(yel2, True)
        GPIO.output(gre2, True)
        GPIO.output(blu2, True)     