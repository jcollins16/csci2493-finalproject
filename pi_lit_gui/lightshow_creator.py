from tkinter import * #import all widgets from tkinter
import tkinter.font   #import font function
from time import sleep #import sleep() function 
from gpiozero import LED #import the gpio zero library to handle individual leds better
import RPi.GPIO #Only using gpio.cleanup() function
from gpiozero import LEDBoard #import gpio array support
from master_lightshow import * #import ALL lightshow functions
import queue #From python library to create queue data structure

leds = LEDBoard(17,26,24,13,4,27,6,5,12) #place active GPIO pins into array from 0-8

root = Tk() #Creating a window
root.title("Lightshow Creator") #project name
myFont = tkinter.font.Font(family = 'Helvetica', size = 12, weight = "bold") #set font as desired


q = queue.Queue() #Creating a queue data structure named q


##Root cleanup Function##
        
def exitToggle():
    RPi.GPIO.cleanup() #set GPIO pins to default(off) state
    root.destroy() #exit window
    
##LIGHTSHOW BUTTON FUNCTIONS FOR PLACING SHOWS INTO A QUEUE##
#Base lightshow files imported from master_lightshow
#Variable 'time' taken from user input box using .get() function
#Input converted from string(Entry command in tkinter) to float
#Float transfered into local 'time' variable and fed into function

#q.put used, with lambda, to place function with a time variable into queue (q)
#.insert command places name of function into queueDisplay text box when button pressed
#lambda creates an anonyoms variable containing a function within the queue(q)

def waveToggle():    
    string_input = input1.get()
    time = float(string_input)
    
    q.put(lambda: wave(leds, time))
    q.put(lambda: revwave(leds, time))
    queueDisplay.insert(END, "Wave ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')
    
def blinkToggle():    
    string_input = input2.get()
    time = float(string_input)
    q.put(lambda: blink(leds, time))   
    queueDisplay.insert(END, "Blink ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')
    
def crissToggle():    
    string_input = input3.get()
    time = float(string_input)
    q.put(lambda: crisscross(leds, time))   
    q.put(lambda: revcriss(leds, time))
    queueDisplay.insert(END, "CrissCross ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')
    
def inchToggle():    
    string_input = input4.get()
    time = float(string_input)
    q.put(lambda: inchworm(leds, time))   
    q.put(lambda: revinch(leds, time))
    queueDisplay.insert(END, "Inchworm ")   
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')
    
def miscToggle():    
    string_input = input5.get()
    time = float(string_input)
    q.put(lambda: misc(leds, time))   
    queueDisplay.insert(END, "Misc ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')

def secondmiscToggle():    
    string_input = input6.get()
    time = float(string_input)
    q.put(lambda: secondmisc(leds, time))   
    queueDisplay.insert(END, "2ndMisc ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')

def doubleToggle():    
    string_input = input7.get()
    time = float(string_input)
    q.put(lambda: double(leds, time))   
    q.put(lambda: revdou(leds, time))
    queueDisplay.insert(END, "Double ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')
    
def cascadeToggle():    
    string_input = input8.get()
    time = float(string_input)
    q.put(lambda: cascade(leds, time))   
    q.put(lambda: revcascade(leds, time))
    queueDisplay.insert(END, "Cascade ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')

def allToggle():
    string_input = input9.get()
    time = float(string_input)
    q.put(lambda: wave(leds, time))   
    q.put(lambda: revwave(leds, time))
    q.put(lambda: blink(leds, time))   
    q.put(lambda: crisscross(leds, time))
    q.put(lambda: revcriss(leds, time))   
    q.put(lambda: wave(leds, time))
    q.put(lambda: revwave(leds, time))   
    q.put(lambda: inchworm(leds, time))
    q.put(lambda: revinch(leds, time))   
    q.put(lambda: misc(leds, time))
    q.put(lambda: secondmisc(leds, time))   
    q.put(lambda: double(leds, time))
    q.put(lambda: revdou(leds, time))   
    q.put(lambda: cascade(leds, time))
    q.put(lambda: blink(leds, time))   
    q.put(lambda: revcascade(leds, time))
    queueDisplay.insert(END, "All ")
    queueDisplay.insert(END, '(' + string_input + ')' + '\n')




##Function used to .get() functions from a queue and play them FIFO

def queuedLightShowToggle():
    while not q.empty():
        lightShow = q.get()
        #print("do I get here"?)
        lightShow()
    queueDisplay.delete('1.0', END)   

#Creating a text widget to display functions currently in the Queue#
#Widget is created in a new panel(window) called top
#This is to avoid resizing issues between rows
    
top = Toplevel()
top.title('Queue View') 
queueDisplay = Text(top, font=myFont, height=20, width=20)
queueDisplay.grid(row =0, column=0)



##Labels for lightshow creator panel##
    
lightLabel = Label(root, text='Lightshow |', font=myFont, bg='white', height=1)
lightLabel.grid(row=0,column=1)
    
timeLabel = Label(root, text='Time(s) |', font=myFont, bg='white', height=1)
timeLabel.grid(row=0, column=2)

actionLabel = Label(root, text='Press to Add', font=myFont, bg='white', height=1)
actionLabel.grid(row=0, column=3)
    
waveLabel = Label(root, text='Wave', font=myFont, bg='white', height=1)
waveLabel.grid(row=1, column=1)
    
blinkLabel = Label(root, text='Blink', font=myFont, bg='white', height=1)
blinkLabel.grid(row=2, column=1)
    
crissLabel = Label(root, text='CrissCross', font=myFont, bg='white', height=1)
crissLabel.grid(row=3, column=1)
    
inchLabel = Label(root, text='InchWorm', font=myFont, bg='white', height=1)
inchLabel.grid(row=4, column=1)
    
miscLabel = Label(root, text='Misc', font=myFont, bg='white', height=1)
miscLabel.grid(row=5, column=1)
    
secondmisLabel = Label(root, text='2nd Misc', font=myFont, bg='white', height=1)
secondmisLabel.grid(row=6, column=1)
    
doubleLabel = Label(root, text='Double', font=myFont, bg='white', height=1)
doubleLabel.grid(row=7, column=1)
    
cascadeLabel = Label(root, text='Cascade', font=myFont, bg='white', height=1)
cascadeLabel.grid(row=8, column=1)
    
allLabel = Label(root, text='All', font=myFont, bg='white', height=1)
allLabel.grid(row=9, column=1)

   

##Add play function tied to specific lightshow##
    
waveButton = Button(root, text='Queue', font=myFont, command= waveToggle, bg='white', height=1)
waveButton.grid(row=1,column=3)
    
blinkButton = Button(root, text='Queue', font=myFont, command= blinkToggle, bg='white', height=1)
blinkButton.grid(row=2,column=3)

crissButton = Button(root, text='Queue', font=myFont, command= crissToggle, bg='white', height=1)
crissButton.grid(row=3,column=3)
    
inchButton = Button(root, text='Queue', font=myFont, command= inchToggle, bg='white', height=1)
inchButton.grid(row=4,column=3)
    
miscButton = Button(root, text='Queue', font=myFont, command= miscToggle, bg='white', height=1)
miscButton.grid(row=5,column=3)
    
secondmiscButton = Button(root, text='Queue', font=myFont, command= secondmiscToggle, bg='white', height=1)
secondmiscButton.grid(row=6,column=3)
    
doubleButton = Button(root, text='Queue', font=myFont, command= doubleToggle, bg='white', height=1)
doubleButton.grid(row=7,column=3)
    
cascadeButton = Button(root, text='Queue', font=myFont, command= cascadeToggle, bg='white', height=1)
cascadeButton.grid(row=8,column=3)

allButton = Button(root, text='Queue', font=myFont, command= allToggle, bg='white', height=1)
allButton.grid(row=9,column=3)



#Button to trigger the Queue to play#

playQueueButton = Button(root, text='Play Queue', font=myFont, command= queuedLightShowToggle, bg='white', height=1)
playQueueButton.grid(row=1,column=4)


##Add user input areas##
#Input is = time
    
input1 = Entry(root, font=myFont, width=5)
input1.grid(row=1,column=2)
    
input2 = Entry(root, font=myFont, width=5)
input2.grid(row=2,column=2)
    
input3 = Entry(root, font=myFont, width=5)
input3.grid(row=3,column=2)
    
input4 = Entry(root, font=myFont, width=5)
input4.grid(row=4,column=2)
    
input5 = Entry(root, font=myFont, width=5)
input5.grid(row=5,column=2)
    
input6 = Entry(root, font=myFont, width=5)
input6.grid(row=6,column=2)
    
input7 = Entry(root, font=myFont, width=5)
input7.grid(row=7,column=2)
    
input8 = Entry(root, font=myFont, width=5)
input8.grid(row=8,column=2)
    
input9 = Entry(root, font=myFont, width=5)
input9.grid(row=9,column=2)
    

    


##Clean Up Widget##
#Adds the exit button, ties it to exitToggle for GPIO cleanup

exitButton = Button(root, text = 'Exit', font = myFont, command = exitToggle, bg = 'bisque2', height = 1, width = 6)
exitButton.grid(row=9,column=4)


root.protocol("WM_DELETE_WINDOW", exitToggle) #exit using native(upper right) escape button

root.mainloop() #Telling the GUI to loop indefinitly




