def wave(leds):
    leds[0].on()
    sleep(.100)
    leds[0].off()
    leds[1].on()
    sleep(.100)
    leds[1].off()
    leds[2].on()
    sleep(.100)
    leds[2].off()
    leds[3].on()
    sleep(.100)
    leds[3].off()
    leds[4].on()
    sleep(.100)
    leds[4].off()
    leds[5].on()
    sleep(.100)
    leds[5].off()
    leds[6].on()
    sleep(.100)
    leds[6].off()
    leds[7].on()
    sleep(.100)
    leds[7].off()
    leds[8].on()
    sleep(.100)
    leds[8].off() 

def revwave(leds):
    leds[8].on()
    sleep(.100)
    leds[8].off()
    leds[7].on()
    sleep(.100)
    leds[7].off()
    leds[6].on()
    sleep(.100)
    leds[6].off()
    leds[5].on()
    sleep(.100)
    leds[5].off()
    leds[4].on()
    sleep(.100)
    leds[4].off()
    leds[3].on()
    sleep(.100)
    leds[3].off()
    leds[2].on()
    sleep(.100)
    leds[2].off()
    leds[1].on()
    sleep(.100)
    leds[1].off()
    leds[0].on()
    sleep(.100)
    leds[0].off()
    

#douled = LEDBoard(red=LEDBoard(17,27), yellow=LEDBoard(26,6), green=LEDBoard(24,5), blue=LEDBoard (13,12), white=LEDBoard(4)) #control both colored leds simultaniously

def double(douled, time): #function to turn both colors on/off simultaniously
    douled.red.on() ## both reds on
    sleep(time)
    douled.yellow.on()  # both yellows on
    sleep(time)
    douled.green.on()  # both greens on
    sleep(time)
    douled.blue.on() #both blues on
    sleep(time)
    douled.white.on() #middle white on
    sleep(time)
    douled.off() #turn everything off
    
def revdouble(douled, time):
    douled.white.on() #turn middle white on
    sleep(time)
    douled.blue.on() # both blues on
    sleep(time)
    douled.green.on() # both greens on
    sleep(time)
    douled.yellow.on()  # both yellows on
    sleep(time)
    douled.red.on() #both reds on
    sleep(time)
    douled.off()