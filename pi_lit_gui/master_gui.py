from tkinter import * #import all widgets from tkinter
import tkinter.font   #import font function
from time import sleep #import sleep() function 
from gpiozero import LED #import the gpio zero library to handle individual leds better
import RPi.GPIO #Only using gpio.cleanup() function
from gpiozero import LEDBoard #import gpio array support
from master_lightshow import * #import ALL lightshow functions


RPi.GPIO.setmode(RPi.GPIO.BCM) #Used to setup pins for gpio.cleanup()


## hardware ##

#BCM GPIO positions with color coding
#red1 = LED(17)
#yel1 = LED(26)
#gre1 = LED(24)
#blu1 = LED(13)
#whi1 = LED(4)
#red2 = LED(27)
#yel2 = LED(6)
#gre2 = LED(5)
#blu2 = LED(12)

leds = LEDBoard(17,26,24,13,4,27,6,5,12) #place active GPIO pins into array from 0-8


## GUI definitinos ##

root = Tk() #Creating a window
root.title("Pi-lit") #project name
myFont = tkinter.font.Font(family = 'Helvetica', size = 12, weight = "bold") #set font as desired



##EVENT FUNCTIONS##


##Single LED Functions##
#Each Toggle function creates a button with related action
#defualt button text starts at ON, changes to off when button is pressed/=leds.off()

def red1Toggle():
    if leds[0].is_lit:
        leds[0].off()
        red1Button["text"] = "Turn Red1 On"
    else:
        leds[0].on()
        red1Button["text"] = "Turn Red1 Off"
        
def yel1Toggle():
    if leds[1].is_lit:
        leds[1].off()
        yel1Button["text"] = "Turn Yellow1 On"
    else:
        leds[1].on()
        yel1Button["text"] = "Turn Yellow1 Off"
        
def gre1Toggle():
    if leds[2].is_lit:
        leds[2].off()
        gre1Button["text"] = "Turn Green1 On"
    else:
        leds[2].on()
        gre1Button["text"] = "Turn Green1 Off"

def blu1Toggle():
    if leds[3].is_lit:
        leds[3].off()
        blu1Button["text"] = "Turn Blue1 On"
    else:
        leds[3].on()
        blu1Button["text"] = "Turn Blue1 Off"

def whi1Toggle():
    if leds[4].is_lit:
        leds[4].off()
        whi1Button["text"] = "Turn White1 On"
    else:
        leds[4].on()
        whi1Button["text"] = "Turn White1 Off"

def red2Toggle():
    if leds[5].is_lit:
        leds[5].off()
        red2Button["text"] = "Turn red2 On"
    else:
        leds[5].on()
        red2Button["text"] = "Turn red2 Off"

def yel2Toggle():
    if leds[6].is_lit:
        leds[6].off()
        yel2Button["text"] = "Turn Yellow 2 On"
    else:
        leds[6].on()
        yel2Button["text"] = "Turn Yellow 2 Off"

def gre2Toggle():
    if leds[7].is_lit:
        leds[7].off()
        gre2Button["text"] = "Turn Green2 On"
    else:
        leds[7].on()
        gre2Button["text"] = "Turn Green2 Off"

def blu2Toggle():
    if leds[8].is_lit:
        leds[8].off()
        blu2Button["text"] = "Turn Blue2 On"
    else:
        leds[8].on()
        blu2Button["text"] = "Turn Blue2 Off"


##Root cleanup Function##
        
def exitToggle():
    RPi.GPIO.cleanup() #set GPIO pins to default(off) state
    root.destroy() #exit window


## WIDGETS/BUTTONS ##


##Single LED Widgets##
#Creates buttons and style for individual LED manipulation
#uses the .grid system to align the buttons


red1Button = Button(root, text = 'Turn Red1 On', font = myFont, command = red1Toggle, bg = 'white', height = 1, width = 24)
red1Button.grid(row=0,column=1)

yel1Button = Button(root, text = 'Turn Yellow1 On', font = myFont, command = yel1Toggle, bg = 'white', height = 1, width = 24)
yel1Button.grid(row=1,column=1)

gre1Button = Button(root, text = 'Turn Green1 On', font = myFont, command = gre1Toggle, bg = 'white', height = 1, width = 24)
gre1Button.grid(row=2,column=1)

blu1Button = Button(root, text = 'Turn Blue1 On', font = myFont, command = blu1Toggle, bg = 'white', height = 1, width = 24)
blu1Button.grid(row=3,column=1)

whi1Button = Button(root, text = 'Turn White1 On', font = myFont, command = whi1Toggle, bg = 'white', height = 1, width = 24)
whi1Button.grid(row=4,column=1)

red2Button = Button(root, text = 'Turn Red2 On', font = myFont, command = red2Toggle, bg = 'white', height = 1, width = 24)
red2Button.grid(row=5,column=1)

yel2Button = Button(root, text = 'Turn Yellow2 On', font = myFont, command = yel2Toggle, bg = 'white', height = 1, width = 24)
yel2Button.grid(row=6,column=1)

gre2Button = Button(root, text = 'Turn Green2 On', font = myFont, command = gre2Toggle, bg = 'white', height = 1, width = 24)
gre2Button.grid(row=7,column=1)

blu2Button = Button(root, text = 'Turn Blue2 On', font = myFont, command = blu2Toggle, bg = 'white', height = 1, width = 24)
blu2Button.grid(row=8,column=1)



##Second window/lightshow Commander setup##
#Define button to access Commander; linked to root window
#Second windows in tkinter are called Top

def open():
    top = Toplevel()
    top.title('Lightshow Commander')  
    
##LIGHTSHOW BUTTON FUNCTIONS FOR SECOND(top) WINDOW##
#Base lightshow files imported from master_lightshow
#Variable 'time' taken from user input box using .get() function
#Input converted from string(Entry command in tkinter) to float
#Float transfered into local 'time' variable and fed into function
    
    def waveToggle():    
        string_input = input1.get()
        time = float(string_input)
   
        wave(leds, time)
        revwave(leds, time)
    
    def blinkToggle():    
        string_input = input2.get()
        time = float(string_input)
   
        blink(leds, time)
    
    def crissToggle():    
        string_input = input3.get()
        time = float(string_input)
   
        crisscross(leds, time)
        revcriss(leds, time)
    
    def inchToggle():    
        string_input = input4.get()
        time = float(string_input)
   
        inchworm(leds, time)
        revinch(leds, time)
    
    def miscToggle():    
        string_input = input5.get()
        time = float(string_input)
   
        misc(leds, time)
    
    def secondmiscToggle():    
        string_input = input6.get()
        time = float(string_input)
   
        secondmisc(leds, time)
    
    def doubleToggle():    
        string_input = input7.get()
        time = float(string_input)
   
        double(leds, time)
        revdou(leds, time)
    
    
    def cascadeToggle():    
        string_input = input8.get()
        time = float(string_input)
   
        cascade(leds, time)
        revcascade(leds, time)
    
    def allToggle():
        string_input = input9.get()
        time = float(string_input)
        
        wave(leds, time)
        revwave(leds, time)
        blink(leds, time)
        crisscross(leds, time)
        revcriss(leds, time)
        wave(leds, time)
        revwave(leds, time)
        inchworm(leds, time)
        revinch(leds, time)
        misc(leds, time)
        secondmisc(leds, time)
        double(leds, time)
        revdou(leds, time)
        cascade(leds, time)
        revcascade(leds, time)
    
##Exit function for lightshow Commander(top), does not clear gpio setup##
    
    def exitCommanderToggle():
        top.destroy() #exit top window
    
##Labels for second(top) window##
    
    lightLabel = Label(top, text='Lightshow |', font=myFont, bg='white', height=1)
    lightLabel.grid(row=0,column=1)
    
    timeLabel = Label(top, text='Time(s) |', font=myFont, bg='white', height=1)
    timeLabel.grid(row=0, column=2)

    actionLabel = Label(top, text='Press to Start', font=myFont, bg='white', height=1)
    actionLabel.grid(row=0, column=3)
    
    waveLabel = Label(top, text='Wave', font=myFont, bg='white', height=1)
    waveLabel.grid(row=1, column=1)
    
    blinkLabel = Label(top, text='Blink', font=myFont, bg='white', height=1)
    blinkLabel.grid(row=2, column=1)
    
    crissLabel = Label(top, text='CrissCross', font=myFont, bg='white', height=1)
    crissLabel.grid(row=3, column=1)
    
    inchLabel = Label(top, text='InchWorm', font=myFont, bg='white', height=1)
    inchLabel.grid(row=4, column=1)
    
    miscLabel = Label(top, text='Misc', font=myFont, bg='white', height=1)
    miscLabel.grid(row=5, column=1)
    
    secondmisLabel = Label(top, text='2nd Misc', font=myFont, bg='white', height=1)
    secondmisLabel.grid(row=6, column=1)
    
    doubleLabel = Label(top, text='Double', font=myFont, bg='white', height=1)
    doubleLabel.grid(row=7, column=1)
    
    cascadeLabel = Label(top, text='Cascade', font=myFont, bg='white', height=1)
    cascadeLabel.grid(row=8, column=1)
    
    allLabel = Label(top, text='All', font=myFont, bg='white', height=1)
    allLabel.grid(row=9, column=1)

##Add play function tied to specific lightshow##
    
    waveButton = Button(top, text='Play', font=myFont, command= waveToggle, bg='white', height=1)
    waveButton.grid(row=1,column=3)
    
    blinkButton = Button(top, text='Play', font=myFont, command= blinkToggle, bg='white', height=1)
    blinkButton.grid(row=2,column=3)

    crissButton = Button(top, text='Play', font=myFont, command= crissToggle, bg='white', height=1)
    crissButton.grid(row=3,column=3)
    
    inchButton = Button(top, text='Play', font=myFont, command= inchToggle, bg='white', height=1)
    inchButton.grid(row=4,column=3)
    
    miscButton = Button(top, text='Play', font=myFont, command= miscToggle, bg='white', height=1)
    miscButton.grid(row=5,column=3)
    
    secondmiscButton = Button(top, text='Play', font=myFont, command= secondmiscToggle, bg='white', height=1)
    secondmiscButton.grid(row=6,column=3)
    
    doubleButton = Button(top, text='Play', font=myFont, command= doubleToggle, bg='white', height=1)
    doubleButton.grid(row=7,column=3)
    
    cascadeButton = Button(top, text='Play', font=myFont, command= cascadeToggle, bg='white', height=1)
    cascadeButton.grid(row=8,column=3)

    allButton = Button(top, text='Play', font=myFont, command= allToggle, bg='white', height=1)
    allButton.grid(row=9,column=3)
    
##Add user input areas##
#Input is = time
    
    input1 = Entry(top, font=myFont, width=5)
    input1.grid(row=1,column=2)
    
    input2 = Entry(top, font=myFont, width=5)
    input2.grid(row=2,column=2)
    
    input3 = Entry(top, font=myFont, width=5)
    input3.grid(row=3,column=2)
    
    input4 = Entry(top, font=myFont, width=5)
    input4.grid(row=4,column=2)
    
    input5 = Entry(top, font=myFont, width=5)
    input5.grid(row=5,column=2)
    
    input6 = Entry(top, font=myFont, width=5)
    input6.grid(row=6,column=2)
    
    input7 = Entry(top, font=myFont, width=5)
    input7.grid(row=7,column=2)
    
    input8 = Entry(top, font=myFont, width=5)
    input8.grid(row=8,column=2)
    
    input9 = Entry(top, font=myFont, width=5)
    input9.grid(row=9,column=2)
    
##Cleanup button for lightshow(top) window##
    
    exitCommanderButton = Button(top, text = 'Exit', font = myFont, command = exitCommanderToggle, bg = 'bisque2', height = 1, width = 6)
    exitCommanderButton.grid(row=10,column=1)
 
##Buttons to trigger second window from root window##

lightshowButton = Button(root, text='Play Lightshows', command=open)
lightshowButton.grid(row = 10, column = 1)

##Clean Up Widget##
#Adds the exit button, ties it to exitToggle for GPIO cleanup

exitButton = Button(root, text = 'Exit', font = myFont, command = exitToggle, bg = 'bisque2', height = 1, width = 6)
exitButton.grid(row=10,column=0)


root.protocol("WM_DELETE_WINDOW", exitToggle) #exit using native(upper right) escape button

root.mainloop() #Telling the GUI to loop indefinitly

