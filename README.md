# Final Project (Pi_Lit)

Pi_lit is a personal project. 
This project consists of a custom Raspberry Pi powered LED light system, with (9) addressable multicolored LEDs
and a host of related programs which allow direct, individual manipulation of LEDs.
In addition, it also contains a plethora of preprogrammed 'light shows' to demosntrate functionality.
Software to hardware manipulation is achieved using the gpiozero and RPi.GPIO libraries to directly control each General Purpose Input Output (GPIO) pin available on the Pi.

The project is programmed entirely in the Python language, version 3. 

There are 3 different kinds of interfaces to directly access the pi_lit system:
- CLI
- GUI
- Digital Assistant with Voice Commands.

## Hardware
The hardware involved:
- Raspberry Pi 3 B+;
- Raspiaudio MIC +; 
- Breadboard
- (9) colored LEDs
- (9) 330 ohm resistors
- (9) male to female jumper wires

## Hardware Arrangment
The (9) addressable LEDs are arranged using the standard Broadcom pin number (BCM) scheme.
Pin numbers were choosen to not interfere with pins already in use by the Raspiaudio Pi HAT.

- red1 = PIN(17)
- yel1 = PIN(26)
- gre1 = PIN(24)
- blu1 = PIN(13)
- whi1 = PIN(4)
- red2 = PIN(27)
- yel2 = PIN(6)
- gre2 = PIN(5)
- blu2 = PIN(12)

When placed in an array, the pins are ordered from 0-8:
- leds = LEDBoard(17,26,24,13,4,27,6,5,12)

## Software 
The bulk of the pi_lit project resides in the software required to access physical hardware. 
The challange of this project was researching, designing, and implementing GPIO access across the 3 interfaces used.


### Command-line Interface Access
Individual LED's can be accessed using the Command Line Interface.
Additonally, all preprogrammed lightshows can be accessed via the CLI.

To access this functionality, navigate into the pi_lit_cli folder.

CLI functionality for individual LED manipuatlion is accessed via the master_cli.py file.

master_cli.py uses an LED name and an argv command (LEDname on; LEDname off).

```bash
master_cli.py red1 on
master_cli.py red1 off
```
CLI functionality for lightshow demonstration is accessed via the lightshow_cli.py file.

ligthshow_cli.py uses the name of a lightshow as an argv command.

```bash
lightshow_cli.py wave
lightshow_cli.py blink
```

### Graphical User Interface Access
This project also implements the Tkinter Python library to create a Graphical User Interface.
The GUI can be accessed via the Raspberry Pi's Desktop functionality or by using a Virtual Network Connection to SSH directly into the Pi's desktop functionality.
To access the GUI, navigate to the pi_lit_gui folder in the git repository. 

In a desktop environment, access the standard GUI using:
```bash
python3 master_gui.py
```
The first panel offers individual LED buttons. 

Pressing the 'Play lightshow' button allows access to lightshow functionality.

Within the lightshow panel, users can access one of the (8) available lightshows.

Additionally, the time(ms) entry box allows users to change the timing(seconds) of each lightshow.

Select a lightshow, enter a time, and press play to perform each lightshow.

In a desktop environment, to access the lightshow_creator GUI, use:
```bash
python3 lightshow_creator.py 
```
The Lightshow Creator uses a queue data structure to enable the creation of custom lightshow combinations. 

Like the standard GUI, the Lightshow Creator GUI contains (8) lightshow functions with an adjustable time variable entry field.

Instead of a standard 'Play' button, the creator has a 'Queue' button, which adds the lightshow to the queue; and a 'Play Queue' button.

Choose a lightshow, enter a time (seconds), and press 'Queue' to add it to the custom configuration. The chosen function will appear in the 'Queue View' panel.

You can add as many combinations as desired. Once ready, press the 'Play Queue' button to perform the custom lightshow combination.

The 'Queue View' text box will reset after each custom show is performed. 


### Google Voice Assistant Access
This project uses the Google aiy.cloudspeech API to directly manipulate LEDs.
This API converts spoken language into text, and text is converted into commands which triggers actions written in software.
To begin, navigate to the pi_lit_voice directory.

To access voice functionality, use:
```bash
python3 pi_lit_voice.py
```
LED and lightshow manipulation is accessed via predetermined voice commands.
For example, individual LEDs can be turn on and off by speaking the LEDs name and a command:
```bash
red 1 on
red 1 off
```

In the same way, light shows can be accessed by speaking the lightshow's name:
```bash
wave
cascade
```

## Preprogrammed 'Lightshows'
Their are, by default, (8) available preprogrammed lightshows contained in the pi_lit software.
There is also a function called "all" present in all three interfaces which plays all (8) functions together in a single lightshow.
These lightshows are available in the master_ligthshow.py files in each directory.
The only exception is the master_lightshow contained in the gui directory. This file offers adjustable arguments for a sleep(time) variable. 
When accessed by one of the interfaces, many of the functions also perform a reverse lightshow function of the same type.  
These are not directly accessible in the intefaces to ensure consistancy and streamline appearance within each interface. 

The lightshows functions include:
- blink(leds)
- wave(leds)
- crisscross(leds)
- inchworm(leds)
- misc(leds)
- secondmisc(leds)
- double(leds)
- cascade(leds)


## Built With
- Python 3 - Scripting language

- RPi.GPIO - CLI manipulation

- Gpiozero - LED array manipulation

- Google aiy.cloudspeech - Voice activation

- Tkinter - GUI implementation

## Authors

- Jordan Collins



