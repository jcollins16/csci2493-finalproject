#!/usr/bin/env python3

##Master file to control Pi-lit with voice commands##

import argparse #Translate speech into text functionality
import locale #Import English language
import logging #Google is watching

from aiy.board import Board, Led
from aiy.cloudspeech import CloudSpeechClient

from gpiozero import * #Import all gpiozero functionality
from time import sleep #
#import RPi.GPIO as GPIO #

from master_lightshow import * #import all lightshows form master_lightshow file

leds = LEDBoard(17,26,24,13,4,27,6,5,12) #Create LED array using GPIOzero

##get_hints gives hints in shell on what voice commands work ##

def get_hints(language_code):
    if language_code.startswith('en_'):
        return ('red one on',
                'red one off',
                
                'yellow one on',
                'yellow one off',
                
                'green on on',
                'green one off',
                
                'blue one on',
                'blue one off',
                
                'white on',
                'white off',
                
                'red two on',
                'red two off',
                
                'yellow two on',
                'yellow two off',
                
                'green two on',
                'green two off',
                
                'blue two on',
                'blue two off',
                
                'all on',
                'all off',
                
                'wave',
                'blink',
                'crisscross',
                'inch',
                'misc',
                'double',
                'cascade',
                'all shows',
                
                'goodbye')
    return None

##Get english##
def locale_language():
    language, _ = locale.getdefaultlocale()
    return language

##Main function starts google voice services, parses langauge into text, activates commands using said text##

def main():
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='Assistant service example.')
    parser.add_argument('--language', default=locale_language())
    args = parser.parse_args()
    

    logging.info('Initializing for language %s...', args.language)
    hints = get_hints(args.language)
    client = CloudSpeechClient() #Access google cloudspeechAPI
    with Board() as board:
        while True:
            if hints:
                logging.info('Say something, e.g. %s.' % ', '.join(hints))
            else:
                logging.info('Say something.')
            text = client.recognize(language_code=args.language,
                                    hint_phrases=hints)
            if text is None:
                logging.info('You said nothing.')
                continue 

##Voice command triggers##

            logging.info('You said: "%s"' % text) #Gives feedback in shell about which words google voice heard
            text = text.lower()
            
            #voice command for individual LED manipulation
            if 'red one on' in text:
                leds[0].on()
            elif 'red one off' in text:
                leds[0].off()
            
            if 'yellow one on' in text:
                leds[1].on()
            elif 'yellow one off' in text:
                leds[1].off()
            
            if 'green one on' in text:
                leds[2].on()
            elif 'green one off' in text:
                leds[2].off()
                
            if 'blue one on' in text:
                leds[3].on()
            elif 'blue one off' in text:
                leds[3].off()
            
            if 'white on' in text:
                leds[4].on()
            elif 'white off' in text:
                leds[4].off()
            
            if 'red two on' in text:
                leds[5].on()
            elif 'red two off' in text:
                leds[5].off()
                
            if 'yellow two on' in text:
                leds[6].on()
            elif 'yellow two off' in text:
                leds[6].off()
            
            if 'green two on' in text:
                leds[7].on()
            elif 'green two off' in text:
                leds[7].off()
                
            if 'blue two on' in text:
                leds[8].on()
            elif 'blue two off' in text:
                leds[8].off()
            
            if 'all on' in text:
                leds.value = (1,1,1,1,1,1,1,1,1)
            elif 'all off' in text:
                leds.value = (0,0,0,0,0,0,0,0,0)
            
            #voice commands for lightshow functions
            if 'wave' in text:
                wave(leds)
                revwave(leds)
            
            if 'blink' in text:
                blink(leds)
            
            if 'crisscross' in text:
                crisscross(leds)
                revcriss(leds)
            
            if 'inch' in text:
                inchworm(leds)
                revinch(leds)
            
            if 'miscellaneous' in text:
                misc(leds)
                secondmisc(leds)
            
            if 'double' in text:
                double(leds)
                revdou(leds)
            
            if 'cascade' in text:
                cascade(leds)
                revcascade(leds)
            
            if 'all shows' in text:
                wave(leds)
                revwave(leds)
                blink(leds)
                crisscross(leds)
                revcriss(leds)
                inchworm(leds)
                revinch(leds)
                misc(leds)
                secondmisc(leds)
                double(leds)
                revdou(leds)
                cascade(leds)
                revcascade(leds)
            
            elif 'goodbye' in text:
                break

if __name__ == '__main__':
    main()
